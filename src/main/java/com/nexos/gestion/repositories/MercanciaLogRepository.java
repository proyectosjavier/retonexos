package com.nexos.gestion.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nexos.gestion.models.MercanciaLogModel;
import com.nexos.gestion.models.MercanciaModel;

@Repository
public interface MercanciaLogRepository
		extends JpaRepository<MercanciaLogModel, Integer>, JpaSpecificationExecutor<MercanciaLogModel> {

	@Query(nativeQuery = true, value = "select mer.id, mer.nombreproducto, mer.fechaingreso fechaingreso, mer.cantidad, mer.documentoregistro, us.documento from mercancia_log as mer \r\n"
			+ "inner join usuario as us on mer.documentoregistro=us.id where mercanciafk= :mercanciafk order by id desc ;")
	ArrayList<Object[]> allMercancia(@Param("mercanciafk") Integer mercanciafk);

	@Query(nativeQuery = true, value = "select * from mercancia_log where nombreproducto like :nombreProducto limit 1;")
	MercanciaLogModel findByNombreProducto(@Param("nombreProducto") String nombreProducto);
	
	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "delete from mercancia_log where mercanciafk = :mercanciafk ;")
	void deleteMercanciafk(@Param("mercanciafk") Integer mercanciafk);
}
