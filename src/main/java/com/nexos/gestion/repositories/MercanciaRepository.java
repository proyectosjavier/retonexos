package com.nexos.gestion.repositories;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nexos.gestion.models.MercanciaModel;

@Repository
public interface MercanciaRepository
		extends JpaRepository<MercanciaModel, Integer>, JpaSpecificationExecutor<MercanciaModel> {

	@Query(nativeQuery = true, value = "select mer.id, mer.nombreproducto, mer.fechaingreso fechaingreso, mer.cantidad, mer.documentoregistro, us.documento from mercancia as mer \r\n"
			+ "inner join usuario as us on mer.documentoregistro=us.id;")
	ArrayList<Object[]> allMercancia();

	@Query(nativeQuery = true, value = "select * from mercancia where nombreproducto like :nombreProducto limit 1;")
	MercanciaModel findByNombreProducto(@Param("nombreProducto") String nombreProducto);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "UPDATE mercancia SET nombreproducto= :nombreProducto, fechaingreso= :fechaingreso, cantidad= :cantidad WHERE id= :id ;")
	int actualizarMercancia(@Param("nombreProducto") String nombreProducto, @Param("fechaingreso") Date fechaingreso,
			@Param("cantidad") Integer cantidad, @Param("id") Integer id);

	@Query(nativeQuery = true, value = "select count(*) from mercancia where documentoregistro = :usuario and id= :id ;")
	int findByMercanciaUsuario(@Param("usuario") Integer usuario, @Param("id") Integer id);
	
	@Query(nativeQuery = true, value = "select count(*) from mercancia where documentoregistro = :usuario and id= :id ;")
	int MercanciaUsuario(@Param("usuario") Integer usuario, @Param("id") Integer id);

}
