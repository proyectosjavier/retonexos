package com.nexos.gestion.repositories;

import com.nexos.gestion.models.UsuarioModel;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioModel, Integer> {

	@Query(nativeQuery = true, value = "select id from usuario where usuario.documento = :usuario ;")
	int findUsuario(@Param("usuario") int usuario);

}
