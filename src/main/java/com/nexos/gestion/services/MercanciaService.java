package com.nexos.gestion.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.nexos.gestion.models.MercanciaModel;
import com.nexos.gestion.repositories.MercanciaRepository;

@Service
public class MercanciaService {
	@Autowired
	MercanciaRepository mercanciaRepository;

	public ArrayList<Object[]> getMercancias() {
		return (ArrayList<Object[]>) mercanciaRepository.allMercancia();
	}

	public MercanciaModel guardarMercancia(MercanciaModel Mercancia) {
		return mercanciaRepository.save(Mercancia);
	}

	public MercanciaModel findByNombreProducto(String nombreProducto) {
		System.out.println(nombreProducto);
		return mercanciaRepository.findByNombreProducto(nombreProducto);
	}

	public Optional<MercanciaModel> findByIdMercancia(int nombreProducto) {
		return mercanciaRepository.findById(nombreProducto);
	}

	public void eliminarMercancia(MercanciaModel mercancia) {
		mercanciaRepository.delete(mercancia);
	}

	public int actualizarMercancia(MercanciaModel mercancia) {
		return mercanciaRepository.actualizarMercancia(mercancia.getNombreproducto(), mercancia.getFechaingreso(),
				mercancia.getCantidad(), mercancia.getId());
	}

	public int findByMercanciaUsuario(MercanciaModel mercancia) {
		return mercanciaRepository.findByMercanciaUsuario(mercancia.getDocumentoregistro(), mercancia.getId());
	}

}
