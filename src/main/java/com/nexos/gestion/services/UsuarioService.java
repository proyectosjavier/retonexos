package com.nexos.gestion.services;

import java.util.ArrayList;

import com.nexos.gestion.models.UsuarioModel;
import com.nexos.gestion.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
	@Autowired
	UsuarioRepository usuarioRepository;

	public ArrayList<UsuarioModel> getUsuarios() {
		return (ArrayList<UsuarioModel>) usuarioRepository.findAll();
	}

	public UsuarioModel guardarUsuario(UsuarioModel Usuario) {
		return usuarioRepository.save(Usuario);
	}

	public int findUsuario(int usuario) {
		int id = 0;
		try {
			id = usuarioRepository.findUsuario(usuario);
		} catch (Exception e) {
			System.out.println("error findUsuario " + e);
		}
		return id;

	}
}
