package com.nexos.gestion.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.gestion.models.MercanciaLogModel;
import com.nexos.gestion.models.MercanciaModel;
import com.nexos.gestion.repositories.MercanciaLogRepository;
import com.nexos.gestion.repositories.MercanciaRepository;

@Service
public class MercanciaLogService {
	@Autowired
	MercanciaLogRepository mercanciaRepository;

	public ArrayList<Object[]> getMercancias(Integer mercanciafk) {
		return (ArrayList<Object[]>) mercanciaRepository.allMercancia(mercanciafk);
	}

	public MercanciaLogModel guardarMercancia(MercanciaLogModel Mercancia) {
		return mercanciaRepository.save(Mercancia);
	}

	public MercanciaLogModel findByNombreProducto(String nombreProducto) {
		return mercanciaRepository.findByNombreProducto(nombreProducto);
	}

	public Optional<MercanciaLogModel> findByIdMercancia(int nombreProducto) {
		return mercanciaRepository.findById(nombreProducto);
	}

	public void eliminarMercancia(Integer mercanciafk) {
		mercanciaRepository.deleteMercanciafk(mercanciafk);
	}

}
