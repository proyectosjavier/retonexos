package com.nexos.gestion.controllers;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.gestion.models.MercanciaLogModel;
import com.nexos.gestion.models.MercanciaModel;
import com.nexos.gestion.models.UsuarioModel;
import com.nexos.gestion.services.MercanciaLogService;
import com.nexos.gestion.services.MercanciaService;
import com.nexos.gestion.services.UsuarioService;

@RestController
@RequestMapping("")
public class MercanciaController {

	@Autowired
	MercanciaService mercanciaService;
	@Autowired
	MercanciaLogService mercanciaLogService;
	@Autowired
	UsuarioService usuarioService;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/mercancias")
	public List<MercanciaModel> getMercancias() {
		List<Object[]> mer = mercanciaService.getMercancias();
		List<MercanciaModel> mercancias = new ArrayList<>();
		try {
			for (Object[] mercanciaModel : mer) {
				MercanciaModel merca = new MercanciaModel();
				merca.setId(Integer.parseInt(mercanciaModel[0].toString()));
				merca.setNombreproducto(mercanciaModel[1].toString());
				merca.setFechaingreso(Date.valueOf(mercanciaModel[2].toString()));
				merca.setCantidad(Integer.parseInt(mercanciaModel[3].toString()));
				merca.setDocumentoregistro(Integer.parseInt(mercanciaModel[5].toString()));
				mercancias.add(merca);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mercancias;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/mercanciaslog")
	public List<MercanciaLogModel> getMercanciasLog(
			@RequestParam(value = "id", defaultValue = "0") Integer mercanciafk) {
		List<Object[]> mer = mercanciaLogService.getMercancias(mercanciafk);
		List<MercanciaLogModel> mercancias = new ArrayList<>();
		try {
			for (Object[] mercanciaModel : mer) {
				MercanciaLogModel merca = new MercanciaLogModel();
				merca.setId(Integer.parseInt(mercanciaModel[0].toString()));
				merca.setNombreproducto(mercanciaModel[1].toString());
				merca.setFechaingreso(Date.valueOf(mercanciaModel[2].toString()));
				merca.setCantidad(Integer.parseInt(mercanciaModel[3].toString()));
				merca.setDocumentoregistro(Integer.parseInt(mercanciaModel[5].toString()));
				mercancias.add(merca);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mercancias;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/addmercancia")
	public String registrarMercancia(@RequestBody MercanciaModel Mercancia) {
		String val = "";
		MercanciaModel valNombreProducto = new MercanciaModel();
		valNombreProducto = mercanciaService.findByNombreProducto(Mercancia.getNombreproducto());
		if (valNombreProducto == null) {
			MercanciaModel mer = this.mercanciaService.guardarMercancia(Mercancia);
			MercanciaLogModel log = new MercanciaLogModel();
			log.setMercanciafk(Mercancia.getId());
			log.setCantidad(Mercancia.getCantidad());
			log.setDocumentoregistro(Mercancia.getDocumentoregistro());
			log.setNombreproducto(Mercancia.getNombreproducto());
			log.setFechaingreso(Mercancia.getFechaingreso());
			this.mercanciaLogService.guardarMercancia(log);
			val = "Producto Registrado: " + mer.getNombreproducto() + "";
		} else {
			val = "Producto Existente";
		}
		return val;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/updatemercancia")
	public String guardarMercancia(@RequestBody MercanciaModel Mercancia) {
		String val = "";
		System.out.println(Mercancia.getId());
		MercanciaLogModel log = new MercanciaLogModel();
		this.mercanciaService.actualizarMercancia(Mercancia);
		log.setMercanciafk(Mercancia.getId());
		log.setCantidad(Mercancia.getCantidad());
		log.setDocumentoregistro(Mercancia.getDocumentoregistro());
		log.setNombreproducto(Mercancia.getNombreproducto());
		log.setFechaingreso(Mercancia.getFechaingreso());
		this.mercanciaLogService.guardarMercancia(log);
		return val;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/mercancia")
	public Optional<MercanciaModel> findMercancia(@RequestParam(value = "id", defaultValue = "0") Integer id) {
		return this.mercanciaService.findByIdMercancia(id);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/mercancia")
	public void eliminarMercancia(@RequestBody MercanciaModel mercancia) {
		int val = 0;
		val = mercanciaService.findByMercanciaUsuario(mercancia);
		if (val > 0) {
			MercanciaLogModel log = new MercanciaLogModel();
			log.setMercanciafk(mercancia.getId());
			log.setCantidad(mercancia.getCantidad());
			log.setDocumentoregistro(mercancia.getDocumentoregistro());
			log.setNombreproducto(mercancia.getNombreproducto());
			log.setFechaingreso(mercancia.getFechaingreso());
			mercanciaLogService.eliminarMercancia(mercancia.getId());
			mercanciaService.eliminarMercancia(mercancia);
		}
	}
}
