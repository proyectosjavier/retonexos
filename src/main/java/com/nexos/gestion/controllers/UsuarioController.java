package com.nexos.gestion.controllers;

import java.util.List;

import com.nexos.gestion.models.UsuarioModel;
import com.nexos.gestion.services.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService usuarioService;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping()
    public List<UsuarioModel> getUsuarios() {
        return this.usuarioService.getUsuarios();
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping()
    public UsuarioModel guardarUsuario(@RequestBody UsuarioModel Usuario) {
        return this.usuarioService.guardarUsuario(Usuario);
    }
    
//    @CrossOrigin(origins = "http://localhost:4200")
//    @GetMapping()
//    public List<UsuarioModel> findUsuario(@RequestParam(value = "cedula", defaultValue = "0") Integer cedula) {
//        return this.usuarioService.findUsuario();
//    }
}
