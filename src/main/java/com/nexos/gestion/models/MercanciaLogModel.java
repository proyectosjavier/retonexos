package com.nexos.gestion.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Mercancia_log")
public class MercanciaLogModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "nombreproducto")
	private String nombreproducto;
	@Column(name = "cantidad")
	private Integer cantidad;
	@Column(name = "fechaingreso")
	private Date fechaingreso;
	@Column(name = "documentoregistro")
	private Integer documentoregistro;
	@Column(name = "mercanciafk")
	private Integer mercanciafk;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombreproducto() {
		return nombreproducto;
	}

	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(Date fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public Integer getDocumentoregistro() {
		return documentoregistro;
	}

	public void setDocumentoregistro(Integer documentoregistro) {
		this.documentoregistro = documentoregistro;
	}

	public Integer getMercanciafk() {
		return mercanciafk;
	}

	public void setMercanciafk(Integer mercanciafk) {
		this.mercanciafk = mercanciafk;
	}

}
